package app;

import app.util.NumberChecker;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class TransactionAmount {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            throw new IllegalStateException("Should pass 2 arguments");
        }

        if (!NumberChecker.isNumeric(args[0])) {
            throw new IllegalStateException("Argument 1 should be a number");
        }

        if (!args[1].equals("credit") && !args[1].equals("debit")) {
            throw new IllegalStateException("Argument 2 should be either 'credit' or 'debit'");
        }

        var userTransactions = countTransactionAmount(Integer.valueOf(args[0]), args[1]);
        userTransactions.forEach((userId, amount) ->
                System.out.println(String.format("User ID: %s, Amount: %.2f", userId, amount)));
    }

    public static Map<Integer, Float> countTransactionAmount(int locationId, String txnType) throws Exception {
        var url = String.format("https://jsonmock.hackerrank.com/api/transactions/search?txnType=%s", txnType);
        var responseBody = searchTran(url);

        var transaction = new JSONObject(responseBody);
        var data = transaction.getJSONArray("data");
        return findUserAndAmountByLocationId(data, locationId);
    }

    private static String searchTran(String url) throws Exception {
        var objUrl = new URL(url);
        var connection = (HttpURLConnection) objUrl.openConnection();
        connection.setRequestMethod("GET");

        var responseCode = connection.getResponseCode();
        if (responseCode != 200) {
            throw new IllegalStateException(
                    String.format("Transaction API returns HTTP status %s instead of HTTP status 200", responseCode));
        }

        var in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        var inputLine = "";
        var response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private static Map<Integer, Float> findUserAndAmountByLocationId(JSONArray data, int locationId) {
        Map<Integer, Float> returnValue = new HashMap<>();

        for (int index = 0; index < data.length(); index++) {
            var user = data.getJSONObject(index);
            var location = user.getJSONObject("location");
            if (location.getInt("id") == locationId) {
                var userId = user.getInt("userId");
                var amount = convertToNumericAmount(user.getString("amount"));
                if (!returnValue.containsKey(userId)) {
                    returnValue.put(userId, amount);
                } else {
                    var lastAmount = returnValue.get(userId);
                    returnValue.put(userId, lastAmount + amount);
                }
            }
        }

        return returnValue;
    }

    private static float convertToNumericAmount(String amountString) {
        amountString = amountString.replaceAll("[^\\d.]", "");
        return Float.valueOf(amountString);
    }


}
