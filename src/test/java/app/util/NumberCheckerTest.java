package app.util;

import org.junit.Assert;
import org.junit.Test;

public class NumberCheckerTest {

    @Test
    public void should_return_true_when_passing_numeric_string() {
        var strNum = "1";

        var result = NumberChecker.isNumeric(strNum);

        Assert.assertTrue(result);
    }

    @Test
    public void should_return_false_when_passing_non_numeric_string() {
        var strNum = "a";

        var result = NumberChecker.isNumeric(strNum);

        Assert.assertFalse(result);
    }
}
