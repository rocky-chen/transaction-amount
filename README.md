Transaction Statements for Users
---------

Please use the following command to run the application on `JVM 11`
```
./gradlew -q run --args='7 debit' 
```

The first argument is the `location ID` and the last argument is the `transaction type`.

